
let radio = document.getElementById("radios");
let checkbox = document.getElementById("checkboxes");
radio.style.display = "none";
checkbox.style.display = "none";
let count = document.getElementById("count");
function updatePrice() {
    // Находим select по имени в DOM.
    let s = document.getElementsByName("Type");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;   //??
    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }
    // Скрываем или показываем радиокнопки.
    //let radioDiv = document.getElementById("radios");
    radio.style.display = (select.value == "2" ? "block" : "none");
    // Смотрим какая товарная опция выбрана.
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radioNOW) {
        if (radioNOW.checked && select.value == "2") {
            let optionPrice = prices.prodOptions[radioNOW.value];
            if (optionPrice !== undefined) {
             price += optionPrice;
            }
        }
    });
    // Скрываем или показываем чекбоксы.
    //let checkDiv = document.getElementById("checkboxes");
    checkbox.style.display = (select.value == "3" ? "block" : "none");
    // Смотрим какие товарные свойства выбраны.
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkboxNOW) {
        if (checkboxNOW.checked && select.value == "3") {
            let propPrice = prices.prodProperties[checkboxNOW.name];
            if (propPrice !== undefined) {
                price += propPrice;
            }
        }
    });
    return price;
}
function getPrices() {
    return {
        prodTypes: [35, 150, 20],
        prodOptions: {
            option1: 10,  
            option2: 20,
            option3: 25,
        },
        prodProperties: {
            prop1: 12,
            prop2: 5,
        }
    };
}
window.addEventListener('DOMContentLoaded', function (event) { 
    console.log("DOM fully loaded and parsed");
    let d = document.getElementsByName("Type");
    var f1 = parseInt(document.getElementById("count").value);
    var answer;
    var result;
    d[0].addEventListener("change", function(event) { 
        let select = event.target; 
        console.log(select.value);   
        answer=updatePrice();
        //var result;
        result=answer*f1;
        let prodPrice = document.getElementById("answer");
        prodPrice.innerHTML = result + " рублей";
    });
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            let r = event.target;
            console.log(r.value);
            answer=updatePrice();
            //var result;
            result=answer*f1;
            let prodPrice = document.getElementById("answer");
            prodPrice.innerHTML = result + " рублей";
        });
    });
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            let c = event.target;
            console.log(c.name);
            console.log(c.value);
            answer=updatePrice();
            //var result;
            result=answer*f1;
            let prodPrice = document.getElementById("answer");
            prodPrice.innerHTML = result + " рублей";
        });
    });
    count.addEventListener("input", function(event){
        let prodPrice = document.getElementById("answer");
        f1 = document.getElementById("count").value;
        if(!(/^[0-9]+$/.test(f1))){
            prodPrice.innerHTML = "Только целые числа!"
            }
        else{
            answer=updatePrice();
            var result;
            result=f1*answer;
            prodPrice.innerHTML =result + " рублей";
        }
    });
})